package com.ynov;

import com.ynov.model.Payment;
import com.ynov.repository.AccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by arnaudmimart on 20/03/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EndpointTest {

    @MockBean
    private AccountRepository accountRepository;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testVerifError(){
        ResponseEntity<Error> response = restTemplate.postForEntity("/payment", new Payment("", "", 0, -1), Error
                .class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        response = restTemplate.postForEntity("/payment", new Payment("", "", 0, 0), Error.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

        response = restTemplate.postForEntity("/payment", new Payment("", "6666", 0, 0), Error.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        response = restTemplate.postForEntity("/payment", new Payment("", "66", 0, 0), Error.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        response = restTemplate.postForEntity("/payment", new Payment("", "AAA", 0, 0), Error.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

        response = restTemplate.postForEntity("/payment", new Payment("3456", "", 0, 0), Error.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        response = restTemplate.postForEntity("/payment", new Payment("AAAAZZZZEEEE", "", 0, 0), Error.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

}
