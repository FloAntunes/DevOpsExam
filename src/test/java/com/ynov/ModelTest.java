package com.ynov;

import com.ynov.model.Account;
import com.ynov.model.Payment;
import com.ynov.repository.AccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by arnaudmimart on 20/03/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ModelTest {

    @MockBean
    private AccountRepository accountRepository;

    @Test
    public void testAccount(){
        Account account = new Account("123456789012", "666", 1000000000);
        assertEquals("123456789012", account.getCardNumber());
        assertEquals("666", account.getCvv());
        assertEquals(1000000000, account.getValidityDate());
        assertEquals(300, (int) account.getSum());
    }

    @Test
    public void testPayment(){
        Payment payment = new Payment("123456789012", "666", 1000000000, 100);
        assertEquals("123456789012", payment.getCardNumber());
        assertEquals("666", payment.getCvv());
        assertEquals(1000000000, payment.getValidityDate());
        assertEquals(100, (int) payment.getAmount());
    }

}
