package com.ynov.component;

import com.ynov.model.Account;
import com.ynov.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by arnaudmimart on 20/03/2017.
 */
@Component
public class PostStartupComponent {

    @Autowired
    private AccountRepository accountRepository;

    @PostConstruct
    public void fillDatabase(){
        for (int i = 0; i < 10; i++) {
            accountRepository.save(new Account(
                    "12345678901" + i,
                    "666",
                    System.currentTimeMillis() + 1000000000
            ));
        }
    }

}
